package com.example.factory;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class WorkerTest {

    @Test
    public void shouldBeAbleToAssignJob() {
        final Worker Worker = new Worker(20);
        final Job job = new Job("123");

        Worker.assignJob(job);

        assertTrue(Worker.getJobs().contains(job));
    }

    @Test
    public void shouldBeAbleToCompleteJob() {
        final Worker Worker = new Worker(20);
        final Job job = new Job("123");

        Worker.assignJob(job);
        Worker.completeJob(job);

        assertEquals(0, Worker.getJobs().size());
    }

    @Test
    public void shouldAbleToGenerateWorkLoadForAWorker() {
        final Worker Worker = new Worker(20);
        final Job job1 = new Job("1");
        final Job job2 = new Job("2");
        final Job job3 = new Job("3");
        Worker.assignJob(job1);
        Worker.assignJob(job2);
        Worker.assignJob(job3);

        assertTrue(Worker.workLoad() == 60);
    }
}
