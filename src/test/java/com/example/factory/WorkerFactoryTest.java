package com.example.factory;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WorkerFactoryTest {
    @Test
    public void shouldAbleToCreateTheEmployee() {
        assertEquals(new Worker(10), Worker.createEmployee());
    }

    @Test
    public void shouldAbleToCreateMachine() {
        assertEquals(new Worker(2), Worker.createMachine());
    }
}
