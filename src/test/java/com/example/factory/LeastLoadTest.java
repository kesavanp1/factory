package com.example.factory;

import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class LeastLoadTest {

    @Test
    public void shouldAssignJobToAWorkerWhoHasLessWorkLoad() throws WorkerOverloadException {
        LeastLoad leastLoad = new LeastLoad();
        Worker worker1 = new Worker(30);
        Worker worker2 = new Worker(15);
        Worker worker3 = new Worker(5);
        Worker worker4 = new Worker(10);

        for (int i = 0; i < 3; i++) {
            worker1.assignJob(new Job("1"));
            worker2.assignJob(new Job("1"));
            worker3.assignJob(new Job("1"));
            worker4.assignJob(new Job("1"));
        }

        ArrayList<Worker> workers = new ArrayList<>();
        workers.add(worker1);
        workers.add(worker2);
        workers.add(worker3);
        workers.add(worker4);
        assertEquals(2, leastLoad.next(workers));
    }

    @Test(expected = WorkerOverloadException.class)
    public void shouldThrowWorkerOverLoadExceptionIfAllTheWorkersHasReachedWorkLimit() throws WorkerOverloadException {
        LeastLoad leastLoad = new LeastLoad();
        Worker worker1 = new Worker(20);
        for (int i = 0; i < 5; i++) {
            worker1.assignJob(new Job("1"));
        }
        ArrayList<Worker> workers = new ArrayList<>();
        workers.add(worker1);

        leastLoad.next(workers);
    }
}