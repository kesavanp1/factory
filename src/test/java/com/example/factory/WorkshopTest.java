package com.example.factory;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class WorkshopTest {

    @Test
    public void shouldRaiseAlertToAllAlertables() {
        Workshop workshop = new Workshop();
        final NotificationReceiver notificationReceiver = mock(NotificationReceiver.class);
        workshop.subscribe(notificationReceiver);
        final Alert alert = new Alert("message", Severity.INFO);
        workshop.sendAlert(alert);
        verify(notificationReceiver).alert(alert);
    }

    @Test
    public void shouldAbleToProcessTheJobUsingStrategy() throws WorkerOverloadException {
        Strategy strategy = mock(Strategy.class);
        Workshop workshop = new Workshop(strategy);
        Job job = new Job("123");

        Worker worker1 = new Worker(10);
        Worker worker2 = new Worker(10);
        Worker worker3 = new Worker(10);

        workshop.addEmployee(worker1);
        workshop.addEmployee(worker2);
        workshop.addEmployee(worker3);

        ArrayList<Worker> workers = new ArrayList<>();
        workshop.process(job);
        when(strategy.next(workers)).thenReturn(0);
        assertTrue(worker1.getJobs().contains(job));
    }
}
