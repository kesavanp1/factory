package com.example.factory;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class RoundRobinTest {
    @Test
    public void shouldAssignTheJobInRoundRobunManner() throws WorkerOverloadException {
        RoundRobin roundRobin = new RoundRobin();
        Worker worker1 = new Worker(10);
        Worker worker2 = new Worker(10);
        Worker worker3 = new Worker(10);

        ArrayList<Worker> workers = new ArrayList<>();
        workers.add(worker1);
        assertEquals(0, roundRobin.next(workers));
        workers.add(worker2);
        assertEquals(1, roundRobin.next(workers));
        workers.add(worker3);
        assertEquals(2, roundRobin.next(workers));

        assertEquals(0, roundRobin.next(workers));
        assertEquals(1, roundRobin.next(workers));

    }

    @Test(expected = WorkerOverloadException.class)
    public void shouldThrowExceptionIfAllWorkersExceedededTheirCapacity() throws WorkerOverloadException {
        RoundRobin roundRobin = new RoundRobin();
        Worker worker1 = new Worker(50);
        Worker worker2 = new Worker(50);
        Worker worker3 = new Worker(50);

        Job job1 = new Job("1");
        Job job2 = new Job("2");
        worker1.assignJob(job1);
        worker1.assignJob(job2);

        Job job3 = new Job("3");
        Job job4 = new Job("4");
        worker2.assignJob(job3);
        worker2.assignJob(job4);

        Job job5 = new Job("5");
        Job job6 = new Job("6");
        worker3.assignJob(job6);
        worker3.assignJob(job5);
        ArrayList<Worker> workers = new ArrayList<>();

        workers.add(worker1);
        workers.add(worker2);
        workers.add(worker3);

        roundRobin.next(workers);

    }
}