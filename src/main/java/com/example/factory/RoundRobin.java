package com.example.factory;

import java.util.List;

public class RoundRobin implements Strategy {
    int next = -1;

    @Override
    public int next(List<Worker> workers) throws WorkerOverloadException {
        int count = 0;
        while (count < workers.size()) {
            next = (next + 1) % workers.size();
            Worker worker = workers.get(next);
            if (worker.workLoad() < 100)
                break;

            count++;
        }
        if (count == workers.size()) {
            throw new WorkerOverloadException();
        }
        return next;
    }
}
