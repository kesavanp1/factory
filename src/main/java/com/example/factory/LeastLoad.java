package com.example.factory;

import java.util.List;

public class LeastLoad implements Strategy {

    @Override
    public int next(List<Worker> workers) throws WorkerOverloadException {
        int next = 0;
        int min = 9999;
        int i;
        boolean flag = false;
        for (i = 0; i < workers.size(); i++) {
            Worker worker = workers.get(i);
            if (min > worker.workLoad() && worker.workLoad() < 100) {
                next = i;
                min = worker.workLoad();
                flag = true;
            }
        }
        if(!flag)
            throw new WorkerOverloadException();
        return next;
    }

}
