package com.example.factory;

import java.util.List;

public interface Strategy {
    int next(List<Worker> workers) throws WorkerOverloadException;
}
