package com.example.factory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class Worker {
    public static Worker createMachine() {
        return new Worker(2);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Worker)) return false;
        Worker worker = (Worker) o;
        return factor == worker.factor &&
                Objects.equals(getJobs(), worker.getJobs());
    }

    @Override
    public int hashCode() {
        return Objects.hash(factor, getJobs());
    }

    protected int factor;
    protected Collection<Job> jobs;

    public Worker(int factor) {
        jobs = new ArrayList<>();
        this.factor = factor;
    }

    public static Worker createEmployee() {
        return new Worker(10);
    }


    public void assignJob(Job job) {
        this.jobs.add(job);
    }

    public Collection<Job> getJobs() {
        return jobs;
    }

    public void completeJob(Job job) {
        this.jobs.remove(job);
    }

    int pendingJobCount() {
        return getJobs().size();
    }

    public int workLoad() {
        return jobs.size() * factor;
    }
}
