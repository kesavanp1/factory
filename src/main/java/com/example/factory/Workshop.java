package com.example.factory;

import java.util.ArrayList;
import java.util.List;

public class Workshop {

    private List<NotificationReceiver> receivers = new ArrayList<>();
    private List<Worker> workers = new ArrayList<>();
    private Strategy strategy;


    public Workshop(Strategy strategy) {
        receivers = new ArrayList<>();
        this.strategy = strategy;
    }

    public Workshop() {
    }

    public void sendAlert(Alert alert) {

        this.receivers.forEach(r -> r.alert(alert));
    }

    public void subscribe(NotificationReceiver notificationReceiver) {
        this.receivers.add(notificationReceiver);
    }

    public void addEmployee(Worker worker) {
        workers.add(worker);
    }

    public void process(Job job) throws WorkerOverloadException {
        workers.get(strategy.next(workers)).assignJob(job);
    }
}
